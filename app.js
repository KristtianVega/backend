// IMPORTS
import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import morgan from "morgan";
import connection_db from './db/connect_db.js';
import routes_user from './routes/users.route.js';
import routes_items from './routes/items.route.js';
import routes_tags from "./routes/tags.route.js";


//Optener varibles de entorno
dotenv.config();
// Crear la aplicacion
const app = express();
// Midleware
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
// Conexion DB
connection_db(process.env.DB_URI)
// Rutas
app.use("/user",routes_user);
app.use("/items",routes_items);
app.use("/tags",routes_tags);

// Inicializacion de la aplicacion
app.listen(process.env.PUERTO,()=>{
    console.log(`Aplicacion ejecutandose en pueto ${process.env.PUERTO}`);
})