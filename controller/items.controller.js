import Item from '../models/items.models.js'

export const get_lst_items = async (request,response) => {
    const UID = request.params.UID;
    try {
        const lst_items = await Item.find({UID:UID});
        return response.status(200).json(lst_items);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo encontrar el item"})
    }
    
}

export const get_item = async (request,response) => {
    const id = request.body.id;
    try {
        const item = await Item.findOne(id);
        return response.status(200).json(item);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo encontrar el item"})
    }
}

export const create_item = async (request,response) => {
    const UID = request.params.UID;
    const body = request.body;
    try {
        //Crea el item
        const item = new Item({
            UID: UID,
            Titulo: body.Titulo,
            Username:body.Username,
            Password:body.Password,
            URL:body.URL,
            Comentario:body.Comentario,
            Tags:body.Tags,
        });
        //guarda elobjeto en la base de datos
        const resultado= await item.save();
        //retorna el resultado
        return response.status(200).json(resultado);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo crear el item"})
    }
}

export const delete_item = async (request,response) => {
    const id = request.query._id;
    try {
        const resultado = await Item.deleteOne(
            {_id:id},
        )
        return response.json(resultado)
    } catch (error) {
        return response.status(500).send({mensaje:"no se pude eliminar el item"})
    }
}

export const edit_item = async (request,response) => {
    const IID = request.params.IID;
    const body = request.body;
    try {
        const resultado = await Item.findByIdAndUpdate(
            {_id:IID},
            body,
            {new: true},
        )
        return response.json(resultado);
    } catch (error) {
        return response.status(500).send({mensaje:"no se pude editar el item"})
        
    }
}