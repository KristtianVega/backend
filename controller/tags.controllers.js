import Tag from "../models/tags.models.js";
import Item from '../models/items.models.js'

export const create_tag = async (request,response) => {
    const UID = request.params.UID;
    const body = request.body;
    try {
        //Crea el item
        const tag = new Tag({
            UID: UID,
            Titulo: body.Titulo,
        });
        //guarda elobjeto en la base de datos
        const resultado= await tag.save();
        //retorna el resultado
        return response.status(200).json(resultado);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo crear el tag"})
    }
}
export const get_tag = async (request,response) => {
    const UID = request.query.UID;
    const Titulo = request.query.Titulo;
    try {
        const lst_items_tag = await Item.find({$and:[{UID:UID},{Tags:[Titulo]}] } )
        console.log(lst_items_tag);
        return response.status(200).json(lst_items_tag);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo encontrar el tag"})
    }
}
export const get_lst_tags = async (request,response) => {
    const UID = request.params.UID;
    try {
        const lst_tags = await Tag.find({UID:UID});
        return response.status(200).json(lst_tags);
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo encontrar el tag"})
    }
}
export const delete_tag = async (request,response) => {
    const id = request.query._id;
    const UID = request.query.UID;
    const Titulo = request.query.Titulo;
    try {
        const resultado = await Tag.deleteOne(
            {_id:id},
        )
        await Item.updateMany(
            {$and:[{UID:UID},{Tags:[Titulo]}] },
            {$pull:{Tags:
                Titulo
            }}
        )
        return response.json(resultado)
    } catch (error) {
        return response.status(500).send({mensaje:"no se pude eliminar el tag"})
    }
}
export const edit_tag = async (request,response) => {
    const id = request.body.tagOrg._id;
    const UID = request.body.tagOrg.UID;
    const Titulo = request.body.tagOrg.Titulo;
    const NewTitulo = request.body.tagNew;
    try {
        const resultado = await Tag.findByIdAndUpdate(
            {_id:id},
            {
                $set:{Titulo:NewTitulo}
            },
            {new: true},
        )
        await Item.updateMany(
            {$and:[{UID:UID},{Tags:[Titulo]}] },
            {$set:{Tags:
                NewTitulo
            }}
        )
        return response.json(resultado)
    } catch (error) {
        return response.status(500).send({mensaje:"no se pude eliminar el tag"})
    }
}
