import User from '../models/user.models.js'
import { encrypt,decrypt } from "../db/crypt.js";

export const login = async (request,response) => {
    const body = request.query;
    console.log(body);
    try {
        //busca el usuario
        const user = await User.findOne({Username:body.Username})
        //guarda elobjeto en la base de datos
        //retorna el resultado
        console.log(user);
        if (user){
            if (decrypt(user.Password) == body.Password){
                return response.status(200).json(user);
            }else{
                return response.status(502).json({mensaje:"No se pudo encontrar el usuario"})
            }
        }else{
            return response.status(501).json({mensaje:"No se pudo encontrar el usuario"})
        }
    } catch (error) {
        return response.status(500).json({mensaje:"No se pudo encontrar el usuario"})
    }
}

export const signup = async (request,response) => {
    const body = request.body;
    try {
        //Crea el usuario
        const user = new User({
            Username:body.Username,
            Password:encrypt(body.Password),
            Nombre:body.Nombre
        });
        //guarda elobjeto en la base de datos
        const resultado= await user.save();
        //retorna el resultado
        return response.status(200).send("Usuario creado");
    } catch (error) {
        return response.status(500).send("No se pudo crear el usuario")
    }
}


