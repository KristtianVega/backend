import mongoose from "mongoose";

const connection_db = (DB_URI) => {
    mongoose.connect(
        DB_URI,
        {
            useUnifiedTopology: true, //quitar advertencias de la consola
            useNewUrlParser: true, // ""
            useFindAndModify: false, //""
        },
        () => {console.log("Se a conectado a la base de datos...");}
    )
}

export default connection_db ;