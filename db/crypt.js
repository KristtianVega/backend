import CryptoJS from 'crypto-js';

// Encrypt
export function encrypt (text){
    const crypt = CryptoJS.AES.encrypt(text, 'secret key 123').toString();
    return crypt
}


// Decrypt
export function decrypt (crypt){
    const bytes  = CryptoJS.AES.decrypt(crypt, 'secret key 123');
    const text = bytes.toString(CryptoJS.enc.Utf8);
    return text
}

