import mongoose  from "mongoose";

//Definicion del esquema
const ItemSchema =  mongoose.Schema(
    {   
        UID:{
            required:true,
            type:String,
        },
        Titulo:{
            required:true,
            type:String,
        },
        Username:{
            required:true,
            type:String,
        },
        Password:{
            required:true,
            type:String,
        },
        URL:{
            required:true,
            type:String,
        },
        Comentario:{
            type:String,
            default:"Sin comentario",
        },
        Tags:{
            type:Array,
            default:["Sin tags"],
        },
    }
);

export default mongoose.model('Item', ItemSchema)