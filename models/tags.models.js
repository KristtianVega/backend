import mongoose  from "mongoose";

//Definicion del esquema
const TagSchema =  mongoose.Schema(
    {   
        UID:{
            required:true,
            type:String,
        },
        Titulo:{
            required:true,
            type:String,
        },
    }
);

export default mongoose.model('Tag', TagSchema)