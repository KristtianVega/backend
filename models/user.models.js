import mongoose  from "mongoose";

//Definicion del esquema
const UserSchema =  mongoose.Schema(
    {
        Username:{
            required:true,
            type:String,
        },
        Password:{
            required:true,
            type:String,
        },
        Nombre:{
            type:String,
        },
    }
);

export default mongoose.model('User', UserSchema)