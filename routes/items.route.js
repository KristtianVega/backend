import express from 'express';
import {create_item,get_lst_items,delete_item,edit_item,get_item} from '../controller/items.controller.js';

const router = express.Router();

router.get("/:UID",get_lst_items)
router.post("/:UID",create_item)
router.get("/",get_item)
router.delete("/",delete_item)
router.put("/:IID",edit_item)

export default router;