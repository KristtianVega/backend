import express from 'express';
import {create_tag,get_lst_tags,delete_tag,edit_tag,get_tag} from '../controller/tags.controllers.js';

const router = express.Router();

router.get("/:UID",get_lst_tags)
router.post("/:UID",create_tag)
router.get("/",get_tag)
router.delete("/",delete_tag)
router.put("/",edit_tag)

export default router;