import express from "express";
import {login,signup} from "../controller/usuarios.controllers.js" 
const router = express.Router();


router.get("/",login);
router.post("/",signup);

export default router;